"""tkpbp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from homepage.views import index
from FeedbackPage.views import feedbackForm
from userpage.views import loginflutter, registFlutter, user_regist, loginpage, logoutuser,json, UserValidation, UserEmailValidation, PasswordValidation, user_regist_admin
from profileuser.views import get_user_data, user_profile
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls'), name='home'),
    path('json/', json, name="json"),
    path('register/', user_regist, name='register'),
    path('register-admin/', user_regist_admin, name='register-admin'),
    path('get-profile/json', get_user_data, name='get-user-data'),
    path('login/', loginpage, name='login'),
    path('logout/', logoutuser, name='logout'),
    path('article/',include('Article.urls'), name='home'),
    path('registervaccine/', include('registervaccine.urls'), name='registervaccine'),
    path('profile/', include('profileuser.urls')),
    path('register/flutterregist', registFlutter, name="flutter-regist"),
    path('login/flutterlogin', loginflutter, name="flutter-login"),
    path('register/validation-username/', csrf_exempt(UserValidation.as_view()), name='validation-username'),
    path('register/validation-email/', csrf_exempt(UserEmailValidation.as_view()), name='validation-email'),
    path('register/validation-password/', csrf_exempt(PasswordValidation.as_view()), name='validation-password'),
    path('FeedbackPage', feedbackForm),
    path('registrasi/', include('kalender.urls')),
    path('randomgenerator/', include('randomgenerator.urls'), name='randomgenerator'),
]
