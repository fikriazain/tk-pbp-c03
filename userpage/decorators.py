from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect

def unauthenticated_user(view_func):
    #check if user is authenticated
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        else:
            return view_func(request, *args, **kwargs)
        
    
    return wrapper_func

def admin_only(view_func):
    #admin only authorization
    def wrapper_function(request, *args, **kwargs):
        group = None
        if request.user.groups.exists():
            group = request.user.groups.all()[0].name
        
        if group == 'admin':
            return view_func(request, *args, **kwargs)
        else:
            return HttpResponseRedirect('/')
    return wrapper_function

def allowed_user(allowed_roles=[]):
    #roles : admin, originaluser
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):

            group = None

            if request.user.groups.exists():
                group = request.user.groups.all()[0].name
            if group in allowed_roles:
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponse('F u')
        return wrapper_func
    return decorator