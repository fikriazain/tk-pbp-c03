from django import forms
from django.db.models import fields
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from .models import OriginalUser


class UserRegist(UserCreationForm):
    class Meta:
        model = User

        fields = [
            'username','email','password1','password2'
        ]

# class AdminRegist(UserCreationForm):
#     class Meta:
#         model = User
#         fields = [
#             'username','email','password1','password2', 'is_superuser'
#         ]