const emailField = document.querySelector("#email");
const feedBackEmailField = document.querySelector(".invalid-email")

emailField.addEventListener("keyup", (e) => {
    console.log('7777',7777);

    const emailVal = e.target.value;
    
    emailField.classList.remove('is-invalid');
    feedBackEmailField.style.display = 'none';

    if(emailVal.length > 0){

        fetch('/register/validation-email/', {
            body: JSON.stringify({email:emailVal}),
            method:'POST',
        })
        .then(res => res.json())
        .then(data => {
            console.log('data', data);
            if(data.email_error){
                emailField.classList.add('is-invalid');
                feedBackEmailField.style.display = 'block';
                feedBackEmailField.style.color = 'red';
                feedBackEmailField.innerHTML = `<p>${data.email_error}<p>`
            }
        })
    }

});