const passwordField = document.querySelector('#password');
const feedBackPasswordField = document.querySelector(".invalid-password")
passwordField.addEventListener('keyup', (e) => {

    const passwordType=e.target.value;

    passwordField.classList.remove('is-invalid');
    feedBackPasswordField.style.display = 'none';

    if(passwordType.length > 0){
        fetch('/register/validation-password/', {
            body: JSON.stringify({password:passwordType}), 
            method:'POST',
        })
        .then(res => res.json())
        .then(data => {
            console.log('data: ', data);
            if(data.password_error){
                passwordField.classList.add('is-invalid');
                feedBackPasswordField.style.display = 'block';
                feedBackPasswordField.style.color = 'red';
                feedBackPasswordField.innerHTML = `<p>${data.password_error}<p>`
            }
        });
    }

});