from django.urls import path
from django.urls.conf import include
from .views import *

app_name = 'registervaccine'

urlpatterns = [
    path('', registrasi, name='registrasi'),
    #path('daftar/<int:event_id>', daftar, name='daftar'),
    path('json', json, name='json'),

    #----------- urls for calendar implementation -----------------------------
    path('calenders/', CalendarView.as_view(), name='index'),
    path('event/new/', create_event, name='event_new'),
    path(
        'event/edit/<int:pk>/', EventEdit.as_view(), name='event_edit'
    ),
    path(
        'event/<int:event_id>/details/', event_details,
        name='event-detail'
    ),
    path(
        'event/<int:pk>/delete', EventDeleteView.as_view(),
        name='event_delete'
    ),
    path(
        'add_eventmember/<int:event_id>', add_eventmember,
        name='add_eventmember'
    ),
    path(
        'event/<int:pk>/remove', EventMemberDeleteView.as_view(),
        name="remove_event"
    ),
    
]