from registervaccine.models import DataPendaftar
from .models import Event
from django import forms
from django.forms import ModelForm, DateInput, TimeInput

#-------------------- Forms for calendar implementation --------------------------

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = ['title', 'description', 'date', 'start_time', 'end_time']
        # datetime-local is a HTML5 input type
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter event title'
            }),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Enter event description'
            }),
            'date': DateInput(
                attrs={'type': 'date', 'class': 'form-control'},
                format='%Y-%m-%d'
            ),
            'start_time': TimeInput(
                attrs={'type': 'time', 'class': 'form-control', 'value': ''},
                format='%H:%M'
            ),
            'end_time': TimeInput(
                attrs={'type': 'time', 'class': 'form-control'},
                format='%H:%M'
            ),
        }
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        # input_formats to parse HTML5 datetime-local input to datetime field
        self.fields['date'].input_formats = ('%Y-%m-%d',)
        self.fields['start_time'].input_formats = ('%H:%M',)
        self.fields['end_time'].input_formats = ('%H:%M',)


class AddMemberForm(forms.ModelForm):
    class Meta:
        model = DataPendaftar
        fields = ['username']
