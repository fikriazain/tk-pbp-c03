from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import generic
from django.http.response import HttpResponseRedirect, HttpResponse
from django.utils.safestring import mark_safe
from django.core import serializers
from django.http import JsonResponse
from django.urls import reverse_lazy, reverse
from django.db.models import Count
from .forms import AddMemberForm
from registervaccine.models import DataPendaftar
from datetime import timedelta, datetime, date
import calendar

from .forms import EventForm


from .models import *
from .utils import Calendar

@login_required(login_url='login')
def registrasi(request):
    context = {}
    context['events'] = Event.objects.order_by('date', 'start_time')
    context['user'] = request.user
    return render(request, 'registrasi.html', context)

def json(request):
    # Pake ini di AJAX, trus diubah jadi object javascript, tinggal dipake
    # buat nampilin yang udah didaftarin di html-nya
    data = serializers.serialize('json', Event.objects.all())
    return HttpResponse(data, content_type="application/json")

#-------------------------- Views for calendar module ---------------------------------------------

def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month


class CalendarView(LoginRequiredMixin, generic.ListView):
    login_url = 'login'
    model = Event
    template_name = 'calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d = get_date(self.request.GET.get('month', None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(withyear=True)
        context['calendar'] = mark_safe(html_cal)
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        return context


@login_required(login_url='signup')
def create_event(request):
    form = EventForm(request.POST or None)
    if request.POST and form.is_valid():
        title = form.cleaned_data['title']
        description = form.cleaned_data['description']
        date = form.cleaned_data['date']
        start_time = form.cleaned_data['start_time']
        end_time = form.cleaned_data['end_time']
        Event.objects.get_or_create(
            user=request.user,
            title=title,
            date=date,
            description=description,
            start_time=start_time,
            end_time=end_time
        )
        return HttpResponseRedirect(reverse('registervaccine:index'))
    return render(request, 'event.html', {'form': form})


class EventEdit(generic.UpdateView):
    model = Event
    fields = ['title', 'description', 'date', 'start_time', 'end_time']
    template_name = 'event.html'


@login_required(login_url='signup')
def event_details(request, event_id):
    event = Event.objects.get(id=event_id)
    eventmember = DataPendaftar.objects.filter(event=event)
    context = {
        'event': event,
        'eventmember': eventmember
    }
    return render(request, 'event-details.html', context)

class EventDeleteView(generic.DeleteView):
    model = Event
    template_name = 'event_delete.html'
    success_url = reverse_lazy('registervaccine:index')

def add_eventmember(request, event_id):
    forms = AddMemberForm()

    if request.method == 'POST':
        forms = AddMemberForm(request.POST)

        if forms.is_valid():
            member = DataPendaftar.objects.filter(event=event_id)
            event = Event.objects.get(id=event_id)
            if member.count() <= 9:
                user = forms.cleaned_data['username']
                DataPendaftar.objects.create(
                    event=event,
                    user=user
                )
                return redirect('registervaccine:index')
            else:
                print('--------------User limit exceed!-----------------')
    context = {
        'form': forms
    }
    return render(request, 'add_member.html', context)


class EventMemberDeleteView(generic.DeleteView):
    model = DataPendaftar
    template_name = 'event_delete.html'
    success_url = reverse_lazy('registervaccine:index')