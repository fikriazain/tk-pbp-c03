# TK PBP C03

# PBP-C 2021/2022

## Nama Anggota
> Muhammad Imam Luthfi Balaka 2006524290  
> Elang Permana 2006520405  
> Denny Johannes Hasea 2006531264  
> Fauzan Andri 2006524593  
> Agastya Kenzo Nayandra 2006535905
> Fikri Aufaa Zain 2006484040  
> Michael Daw Balma 2006520746  (test commit mike)

## Link HerokuApp
[Here's the link](https://covindox.herokuapp.com/)  

## Cerita Aplikasi
Aplikasi yang kami buat adalah sebuah website yang memiliki berbagai fitur
yang berkaitan dengan keseharian di masa pandemi. Beberapa fitur yang akan
ditawarkan oleh website kami diantaranya:
1. Fitur berita dengan status hoax or not hoax 
2. Fitur info jadwal vaksinasi 
3. Fitur artikel informasi panduan terkait pandemi. 
4. Fitur page pengaduan ODP Covid-19 
5. Fitur pendaftaran vaksin 
6. Fitur text generator penyemangat saat pandemi. 

## Daftar Modul
1. Modul berita (Elang Permana)
2. Modul User Profile dan Login/Registration systems (Fikri Aufaa Zain)
3. Modul jadwal vaksinasi (Fauzan Andri)
4. Modul artikel (Denny Johannes Hasea)
5. Modul halaman pangaduan (Agastya Kenzo Nayandra)
6. Modul form pendaftaran (Muhammad Imam Luthfi Balaka)
7. Modul text generator penyemangat (Michael Daw Balma)

## Persona
> !. Guest
>> Masyarakat indonesia yang ingin menggunakan fasilitas pada web covindox seperti daftar vaksin, melihat daftar vaksin, melihat berita tentang covid-19
> 2. Staff - Pengolah database  
>> Orang-orang yang mengatur web dan database
> 3. Admin -  Administrator  
>> Memegan semua authority pada web
