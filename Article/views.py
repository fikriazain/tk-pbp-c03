from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from .forms import PostForm
from .models import Post
from django.views.decorators.csrf import csrf_exempt
import json as j
from django.core import serializers
from django.http.response import HttpResponse
# Create your views here.
def article_index(request,post):
    li = get_object_or_404(Post,URLartikel=post)
    return render(request, 'index.html',{'li':li})

@login_required(login_url='/admin/login/')
def add_article(request):
    context={}
    form = PostForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()

    context['form'] = form
    return render(request, 'add.html', context)

def awal_article(request):
    li = Post.objects.all()
    return render(request,'awal.html',{'li':li})

@csrf_exempt
def getDataArticleAPI(request,waktu):
    if (request.method =='POST'):
        objek = Post.objects.filter(time=waktu)
        if (objek):
            dct = {
                'URLimageArtikel' : (objek.values_list('URLimageArtikel')[0][0]),
                'titleArtikel' : (objek.values_list('titleArtikel')[0][0]),
                'bagianTitle1' : (objek.values_list('bagianTitle1')[0][0]),
                'URLimagePendukung1' : (objek.values_list('URLimagePendukung1')[0][0]),
                'isiBagian1' : (objek.values_list('isiBagian1')[0][0]),
                'URLimagePendukung2' : (objek.values_list('URLimagePendukung2')[0][0]),
                'bagianTitle2' : (objek.values_list('bagianTitle2')[0][0]),
                'isiBagian2' : (objek.values_list('isiBagian2')[0][0]),
                
            }
            return JsonResponse(dct)
        else:
            dct = {}
            return JsonResponse(dct,status=404)

@csrf_exempt
def daftarDataArticleAPI(request):
    data = j.loads(request.body)
    URLimageArtikel = data['URLimageArtikel']
    titleArtikel  = data['titleArtikel']
    bagianTitle1 = data['bagianTitle1']
    URLimagePendukung1 = data['URLimagePendukung1']
    isiBagian1 = data['isiBagian1']
    URLimagePendukung2 = data['URLimagePendukung2']
    bagianTitle2 = data['bagianTitle2']
    isiBagian2 = data['isiBagian2']
    

    dataArtikel = Post(
        URLimageArtikel = URLimageArtikel,
        titleArtikel=titleArtikel,
        bagianTitle1=bagianTitle1,
        URLimagePendukung1=URLimagePendukung1,
        isiBagian1=isiBagian1,
        URLimagePendukung2=URLimagePendukung2,
        bagianTitle2=bagianTitle2,
        isiBagian2=isiBagian2,
        
    )
    dataArtikel.save()
    return JsonResponse({'status':200})

def json(request):
    data = serializers.serialize('json',Post.objects.all())
    return HttpResponse(data, content_type="application/json")
        
