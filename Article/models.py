from django.db import models
from django_extensions.db.fields import AutoSlugField
# Create your models here.
def my_slugify_function(content):
    return content.replace('_', '-').lower()
class Post(models.Model):
    URLimageArtikel = models.URLField()
    titleArtikel = models.CharField(max_length=100)
    bagianTitle1 = models.CharField(max_length=100)
    URLimagePendukung1 = models.URLField()
    isiBagian1 = models.TextField()
    URLimagePendukung2 = models.URLField()
    bagianTitle2 = models.CharField(max_length=100)
    isiBagian2 = models.TextField()
    URLartikel = AutoSlugField(populate_from='titleArtikel',unique=True,slugify_function=my_slugify_function)
    time = models.DateTimeField(auto_now_add=True)
    
