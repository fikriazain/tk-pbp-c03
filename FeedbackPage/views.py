from django.http import JsonResponse
from .forms import FeedbackForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
# Create your views here.
def feedbackForm(request):
    form = FeedbackForm()
    if request.is_ajax():
        form = FeedbackForm(request.POST)
        print(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({
                'message': 'success'
            })
    return render(request, "Feedback_page.html", {"form": form})
