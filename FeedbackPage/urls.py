from django.urls import path
from .views import feedbackForm

urlpatterns = [
    path('FeedbackPage', feedbackForm, name='Feedback'),
]
