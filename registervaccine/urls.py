from django.urls import path
from .views import index, json, delete, daftar, getDataPendaftarAPI, daftarDataPendaftarAPI, get_user_name

app_name = 'registervac'
urlpatterns = [
    path('', index, name='index'),
    path('daftar/<event_id>', daftar, name='daftar'),
    path('json', json, name='json'),
    path('json', json, name='json_vaccine'),
    path('datapendaftarapi/<nama_user>', getDataPendaftarAPI, name='data_pendaftar_API'),
    path('dataapi', daftarDataPendaftarAPI, name='daftar_data_pendaftar_API'),
    path('delete/', delete, name='delete'),
    path('getusername', get_user_name),
]