from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import FormPendaftaran
from django.http.response import HttpResponse
from django.core import serializers
from .models import DataPendaftar
from kalender.models import Event
from django.http import JsonResponse
import datetime
from django.views.decorators.csrf import csrf_exempt
import json as mdjson

# Create your views here.
@login_required(login_url='login')
def index(request):
    context = {}
    form = FormPendaftaran(request.POST or None)
    form.fields['username'].initial = request.user

    tanggal = request.GET.get('tanggal')
    bulan = request.GET.get('bulan')
    tahun = request.GET.get('tahun')

    if (type(tanggal) is str):
        intended_tanggal = tanggal + ' ' + bulan + ' ' + tahun
        form.fields['tanggal_vaksin'].initial = datetime.datetime.strptime(intended_tanggal, '%d %m %Y')

    if (form.is_valid and request.method == 'POST'):
        form.save()

    context['form'] = form
    return render(request, 'register_vaccine.html', context)

def daftar(request, event_id):
    context = {}
    form = FormPendaftaran(request.POST or None)
    event = Event.objects.get(id=event_id)
    waktu = event.start_time.strftime("%H:%M") + " - " + event.end_time.strftime("%H:%M")

    form.fields['username'].initial = request.user
    # Added by Fauzan Andri
    form.fields['event'].initial = event_id
    form.fields['tanggal_vaksin'].initial = event.date
    form.fields['waktu_vaksin'].initial = waktu

    if (form.is_valid and request.method == 'POST'):
        form.save()

    context = {
        'form': form,
        'event': event
    }

    return render(request, 'register_vaccine.html', context)

@csrf_exempt
def getDataPendaftarAPI(request, nama_user):
    # Method ini mengembalikan json yang dibutuhkan sesuai argumen nama_user.
    # Method ini akan dipanggil dari mobile app sebagai semacam A
    if (request.method == 'POST'):
        objek = DataPendaftar.objects.filter(username=str(nama_user))
        if (objek):
            dct = {
                'tanggal_vaksin': (objek.values_list('tanggal_vaksin')[0])[0].strftime('%d-%m-%Y'),
                'waktu_vaksin': (objek.values_list('waktu_vaksin')[0][0]),
                'nik': (str(objek.values_list('NIK')[0][0])),
                'nama': (objek.values_list('name')[0][0]),
                'umur': (str(objek.values_list('age')[0][0])),
            }
            return JsonResponse(dct)
        else:
            dct = {}
            return JsonResponse(dct, status=404)

@csrf_exempt
def daftarDataPendaftarAPI(request):
    # Inspired from https://stackoverflow.com/questions/40059654/python-convert-a-bytes-array-into-json-format/40060181
    
    if (request.method == 'POST'):
        data = mdjson.loads(request.body)
        username = data["username"]
        tanggal_vaksin = data["tanggal_vaksin"]
        waktu_vaksin = data["waktu_vaksin"]
        nik = data["nik"]
        nama = data["nama"]
        usia = data["usia"]
        
        dataPendaftarBaru = DataPendaftar(username = username, 
        tanggal_vaksin = tanggal_vaksin,
        waktu_vaksin = waktu_vaksin,
        NIK = nik,
        name = nama,
        age = usia)
        dataPendaftarBaru.save()

        return JsonResponse({'status': 200})

@login_required(login_url='login')
def json(request):
    # Pake ini di AJAX, trus diubah jadi object javascript, tinggal dipake
    # buat nampilin yang udah didaftarin di html-nya
    data = serializers.serialize('json', DataPendaftar.objects.filter(username=request.user))
    return HttpResponse(data, content_type="application/json")

@login_required(login_url='login')
def get_user_name(request):
    print('masuk sini mas brooo')
    user_name = '[{' + '"username": ' + '"' + str(request.user) + '"' + '}]'
    return HttpResponse(user_name, content_type="application/json")


def delete(request):
    data = list(DataPendaftar.objects.values())
    pk_number = request.GET.get('pk')
    DataPendaftar.objects.filter(id=int(pk_number)).delete()
    return JsonResponse(data, safe=False)