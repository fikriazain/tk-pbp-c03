from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from registervaccine.models import DataPendaftar
from userpage.decorators import unauthenticated_user, admin_only, allowed_user
from userpage.models import OriginalUser
# Create your views here.


def index(request):
    return render(request, 'homepage_index.html')



