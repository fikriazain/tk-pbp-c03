from .models import RandomNote
from django import forms

class FormNote(forms.ModelForm):
    writers = forms.CharField(disabled=True, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))

    isiText = forms.CharField(widget=forms.Textarea(
        attrs={
            'class' : 'form-control'
        }
    ))
    class Meta:
        model = RandomNote
        fields = '__all__'