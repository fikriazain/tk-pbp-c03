from django.urls import path
from .views import index, add_note, json, getDataNoteAPI, daftarDataNoteAPI

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add_note'),
    path('json', json, name='json'),
    path('data-api', daftarDataNoteAPI, name = 'daftarDataNoteAPI'),
    path('data-note-api/<penulis>', getDataNoteAPI, name = 'getDataNoteAPI'),
]