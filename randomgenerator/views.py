import random
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from django.http import JsonResponse
from .forms import FormNote
from .models import RandomNote
from django.views.decorators.csrf import csrf_exempt
import json as jsn

def index(request):
    maks= RandomNote.objects.latest('pk').pk
    getRandom = random.randrange(1, maks+1)
    temp = RandomNote.objects.get(pk=getRandom)
    return render(request, 'randomgenerator.html', {'temp' : temp})

@login_required(login_url='login')
def add_note(request):
    context = {}
    form = FormNote(request.POST or None)
    form.fields['writers'].initial = request.user

    if (form.is_valid and request.method == 'POST') :
        form.save()
    
    context['form'] = form
    return render(request, 'randomadd.html', context)

@login_required(login_url='login')
def json(request) :
    data = serializers.serialize('json', RandomNote.objects.filter(writers=request.user))
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def getDataNoteAPI(request, penulis):
    if (request.method =='POST'):
        objek = RandomNote.objects.filter(writers=penulis)
        if (objek):
            dct = {
                'writers' : (objek.values_list('writers')[0][0]),
                'isiText' : (objek.values_list('isiText')[0][0])
            }
            return JsonResponse(dct)
        else :
            dct = {}
            return JsonResponse(dct, status=404)

@csrf_exempt
def daftarDataNoteAPI(request):
    data = jsn.loads(request.body)
    writers = data['writers']
    isiText = data['isiText']

    dataNote = RandomNote(
        writers = writers,
        isiText = isiText,
    )

    dataNote.save()
    return JsonResponse({'status':200})