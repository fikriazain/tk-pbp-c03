from django.db.models.fields import CharField
from django.forms.widgets import TextInput, Textarea
from userpage.models import OriginalUser
from django.forms import ModelForm


class OriginalUserForm(ModelForm):


    class Meta:
        model = OriginalUser
        fields = '__all__'
        exclude = ['user']

        widgets = {
            'fullname': TextInput(attrs={'class': "fullname",'style':'font-family:Arial;font-size:20px; max-width: 300px; resize:none; padding:8px; border-radius:10px;'}),
            'no_phone': TextInput(attrs={'class': "no_phone",'style':'font-family:Arial;font-size:20px;max-width: 300px; resize:none;padding:8px; border-radius:10px;'}),
            'address': Textarea(attrs={'class': "address",'style': 'font-family:Arial;font-size:20px;max-width: 300px;resize:none; border-radius:10px; padding:8px 8px 0px 8px; height:200px'}),
        }