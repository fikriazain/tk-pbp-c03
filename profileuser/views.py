from django.core.serializers import serialize
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse, JsonResponse
from registervaccine.views import json
from .form import OriginalUserForm
from userpage.models import OriginalUser
from django.http import HttpResponseRedirect
from registervaccine.models import DataPendaftar
from django.contrib.auth.models import Group, User
import datetime
import json as jsons
from ast import literal_eval
from django.core import serializers
import calendar

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime.date(year, month, day)

# Create your views here.
def user_profile(request):
    try:
        profile = request.user.originaluser
        regist = DataPendaftar.objects.get(username=request.user)
        admin = User.objects.get(username=request.user)
        second_date = add_months(regist.tanggal_vaksin, 1)
        context = {'profile':profile, 'regist': regist, 'second_date':second_date}
        return render(request,'profile_index.html', context)
    except:
        profile = request.user.originaluser
        context = {'profile':profile}
        return render(request,'profile_index.html', context)


def edit_profile(request):
    originaluser = request.user.originaluser
    form = OriginalUserForm(instance=originaluser)
    if request.method == 'POST':
        form = OriginalUserForm(request.POST, request.FILES,  instance=originaluser)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/profile')


    context = {'form':form}
    return render(request, 'edit_profile.html', context)

@csrf_exempt
def edit_profile_flutter(request):
    # Inspired from https://stackoverflow.com/questions/40059654/python-convert-a-bytes-array-into-json-format/40060181
    if (request.method == 'POST'):

        data = jsons.loads(request.body)
        fullname = data["fullname"]
        no_phone = data["no_phone"]
        address = data["address"]

        print(request)

        # new_data = OriginalUser.objects.all()
        # target = None
        # n = 0
        # for i in new_data:
        #     print(n)
        #     if i == request.user.originaluser:
        #         target = i
        #         break
        #     n += 1

        request.user.originaluser.fullname = fullname
        request.user.originaluser.no_phone = no_phone
        request.user.originaluser.address = address
        
        request.user.originaluser.save()

        return JsonResponse({'status': 200})


def get_user_data(request):
    data = request.user.originaluser
    datas = OriginalUser.objects.filter(fullname = data)
    fullname = datas.values_list('fullname')[0][0]
    phone = datas.values_list('no_phone')[0][0]
    address = datas.values_list('address')[0][0]

    try:
        regist = DataPendaftar.objects.get(username=request.user)
        second_date = add_months(regist.tanggal_vaksin, 1)
        print(second_date)

        json_data = {
            'fullname': fullname,
            'phone': phone,
            'address': address,
            'vaksin1': regist.tanggal_vaksin,
            'vaksin2': second_date
        }

        return JsonResponse(json_data)



    except:
        json_data = {
            'fullname': fullname,
            'phone': phone,
            'address': address,
            'vaksin1': '',
            'vaksin2': ''
        }

        print(json_data)
        return JsonResponse(json_data)

